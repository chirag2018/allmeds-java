package com.nalashaa.allmeds;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nalashaa.allmeds.model.Login;
import com.nalashaa.allmeds.model.UserInfo;
import com.nalashaa.allmeds.service.IUserInfoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserInfoServiceImplTest {
	
	IUserInfoService userInfoService;
	
	@Test
	public void registerAndLoginUserTest() {
		//Register
		UserInfo userInfo = new UserInfo();
		userInfo.setAddress("1234-Bangalore");
		userInfo.setAllergic("TestAllergy");
		userInfo.setCreatedDate(new Date());
		userInfo.setDob(new Date());
		userInfo.setGender("M");
		userInfo.setId(1234);
		userInfo.setHeight("162");
		userInfo.setName("Test1");
		userInfo.setPassword("abc@1234");
		userInfo.setUserName("Test1");
		userInfo.setWeight("70");
		userInfoService = Mockito.mock(IUserInfoService.class);
		Mockito.when(userInfoService.register(userInfo)).thenReturn(userInfo);
		assertEquals("Test1",userInfoService.register(userInfo).getUserName());
		
		//Login
		Login login = new Login();
		login.setPassword("abc@1234");
		login.setUserName("Test1");
		Mockito.when(userInfoService.login(login)).thenReturn(userInfo);
		assertEquals("Test1",userInfoService.login(login).getUserName());
	}
}
