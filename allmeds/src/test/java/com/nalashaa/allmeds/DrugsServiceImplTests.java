package com.nalashaa.allmeds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nalashaa.allmeds.model.Drugs;
import com.nalashaa.allmeds.model.DrugsDetails;
import com.nalashaa.allmeds.model.DrugsResponse;
import com.nalashaa.allmeds.service.IDrugsService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DrugsServiceImplTests {

	IDrugsService drugsService;
	
	@Test
	public void isValidDrugsTest() {
		System.out.println("Test1");
		//Case 0 : Validating existing drugs.
		Drugs drugs = new Drugs();
		drugs.setDescription("ABCDEFGH");
		drugs.setName("Test1");
		drugs.setId(12345);
		
		DrugsDetails drugsDetails = new DrugsDetails();
		drugsDetails.setDrug(drugs);
		drugsDetails.setId(12345);
		drugsDetails.setQrBarCode("1243434343");
		drugsDetails.setExpiringDate(new Date());
		drugsDetails.setManufacturingDate(new Date());
		drugsDetails.setStripNumber(1234444);
		
		DrugsResponse drugsResponse = new DrugsResponse();
		drugsResponse.setDrugsDetails(drugsDetails);
		drugsResponse.setValid(true);
		drugsService = Mockito.mock(IDrugsService.class);
		Mockito.when(drugsService.isValidDrugs("Test1", "1243434343")).thenReturn(drugsResponse);
		assertEquals(true, drugsService.isValidDrugs("Test1", "1243434343").isValid());
		assertEquals("Test1",drugsService.isValidDrugs("Test1", "1243434343").getDrugsDetails().getDrug().getName());
		
		//Case 0 : Validating invalid drugs.
		DrugsResponse drugsResponse1 = new DrugsResponse();
		drugsResponse1.setValid(false);
		Mockito.when(drugsService.isValidDrugs("Test2", "1243434343")).thenReturn(drugsResponse1);
		assertEquals(false, drugsService.isValidDrugs("Test2", "1243434343").isValid());
	}

}
