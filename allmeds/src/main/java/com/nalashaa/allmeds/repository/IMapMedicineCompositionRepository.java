package com.nalashaa.allmeds.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nalashaa.allmeds.model.MapMedicineComposition;

public interface IMapMedicineCompositionRepository extends JpaRepository<MapMedicineComposition, Long>{

	@Query(value = "FROM MapMedicineComposition mmc WHERE mmc.medicineId = ?1")
	List<MapMedicineComposition> findByMedicineId(long id);

}
