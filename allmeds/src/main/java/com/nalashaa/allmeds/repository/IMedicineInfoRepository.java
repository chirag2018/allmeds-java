package com.nalashaa.allmeds.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nalashaa.allmeds.model.MedicineInfo;

public interface IMedicineInfoRepository extends JpaRepository<MedicineInfo, Long> {
	
	@Query(value = "FROM MedicineInfo mi WHERE mi.id = ?1")
	MedicineInfo findByMedicineId(Long medicineId);
	

}
