package com.nalashaa.allmeds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nalashaa.allmeds.model.DiseaseInfo;

public interface IDiseaseInfoRepository extends JpaRepository<DiseaseInfo, Long>{
	
	DiseaseInfo findByDiseaseName(String diseaseName);

}
