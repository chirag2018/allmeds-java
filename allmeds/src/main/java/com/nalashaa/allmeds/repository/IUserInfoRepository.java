/**
 * 
 */
package com.nalashaa.allmeds.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.nalashaa.allmeds.model.UserInfo;

/**
 * @author ashwanikannojia
 *
 */
@Repository
public interface IUserInfoRepository extends JpaRepository<UserInfo, Long> {

	UserInfo findByUserNameAndPassword(String userName, String password);

	@Query(value = "FROM UserInfo ui WHERE ui.id = ?1")
	UserInfo findByUserId(long userId);

}
