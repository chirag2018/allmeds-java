/**
 * 
 */
package com.nalashaa.allmeds.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nalashaa.allmeds.model.Drugs;

/**
 * @author ashwanikannojia
 *
 */
@Repository
public interface IDrugsRepository extends JpaRepository<Drugs, Long> {

	Drugs findByName(String drugName);

}
