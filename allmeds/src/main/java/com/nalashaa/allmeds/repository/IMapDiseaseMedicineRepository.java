package com.nalashaa.allmeds.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nalashaa.allmeds.model.MapDiseaseMedicine;

public interface IMapDiseaseMedicineRepository extends JpaRepository<MapDiseaseMedicine, Long>{

	List<MapDiseaseMedicine> findByDiseaseId(Long id);

}
