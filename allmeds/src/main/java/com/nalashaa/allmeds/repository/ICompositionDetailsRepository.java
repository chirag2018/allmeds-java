package com.nalashaa.allmeds.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nalashaa.allmeds.model.CompositionDetails;

public interface ICompositionDetailsRepository extends JpaRepository<CompositionDetails, Long> {
	
	@Query(value = "FROM CompositionDetails cd WHERE cd.id = ?1")
	CompositionDetails findByCompositionDetailsId(Long compositionDetailsId);
	

}
