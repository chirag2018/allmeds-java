/**
 * 
 */
package com.nalashaa.allmeds.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nalashaa.allmeds.model.MedicineInfoResponse;
import com.nalashaa.allmeds.service.IMedicineInfoService;

/**
 * @author ashwanikannojia
 *
 */
@RestController
@RequestMapping("/medicine")
public class MedicineInfoController {
	
	private static final Logger logger = LogManager.getLogger(MedicineInfoController.class);
	
	@Autowired
	IMedicineInfoService medicineInfoService;
	
	@CrossOrigin
	@GetMapping(value = "/{userId}/{disease}")
	public MedicineInfoResponse getPrefearbleMedicineInfo(@PathVariable("userId") Long userId,@PathVariable("disease") String disease){
		
		logger.info("ENTERED IN MEDICINE INFO FOR :"+disease);
		return medicineInfoService.findPreferMedicine(userId,disease);
	}
	
}
