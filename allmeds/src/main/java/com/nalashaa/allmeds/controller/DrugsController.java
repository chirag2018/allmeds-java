/**
 * 
 */
package com.nalashaa.allmeds.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nalashaa.allmeds.model.DrugsResponse;
import com.nalashaa.allmeds.service.IDrugsService;

/**
 * @author ashwanikannojia
 *
 */
@RestController
@RequestMapping("/drugs")
public class DrugsController {
	
	private static final Logger logger = LogManager.getLogger(DrugsController.class);
	
	@Autowired
	IDrugsService drugsService;
	
	@CrossOrigin
	@GetMapping(value = "/{drugname}/{qrbarcode}")
	public DrugsResponse isValidDrug(@PathVariable("drugname") String drugName,@PathVariable("qrbarcode") String qrbarcode){
		logger.info("ENTERED IN DRUG VALIDATION FOR :"+drugName);
		return drugsService.isValidDrugs(drugName,qrbarcode);
	}
	
}
