package com.nalashaa.allmeds.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nalashaa.allmeds.model.DiseaseInfo;
import com.nalashaa.allmeds.service.IDiseaseService;

@RestController
@RequestMapping("/disease")
public class DiseaseController {

	
	private static final Logger logger = LogManager.getLogger(DiseaseController.class);
	
	@Autowired
	IDiseaseService diseaseService;
	
	@CrossOrigin
	@GetMapping
	public List<DiseaseInfo> getAllDiseases(){
		logger.info("ENTERED IN DISEASE CONTROLLER FOR ALL DISEASES");
		return diseaseService.getAllDisease();
	}
	
}
