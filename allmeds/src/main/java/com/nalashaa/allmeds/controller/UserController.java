/**
 * 
 */
package com.nalashaa.allmeds.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nalashaa.allmeds.model.Login;
import com.nalashaa.allmeds.model.UserInfo;
import com.nalashaa.allmeds.service.IUserInfoService;

/**
 * @author ashwanikannojia
 *
 */
@RestController
@RequestMapping("/users")
public class UserController {
	
	private static final Logger logger = LogManager.getLogger(UserController.class);
	
	@Autowired
	IUserInfoService userInfoService;
	
	@CrossOrigin
	@PostMapping("/register")
	public UserInfo register(@RequestBody UserInfo userInfo){
		
		logger.info("ENTERED FOR REGISTRATION USER :"+userInfo.getName());
		UserInfo savedUserInfo =userInfoService.register(userInfo);
		return savedUserInfo;	
	}
	
	@CrossOrigin
	@PostMapping("/login")
	public UserInfo login(@RequestBody Login loginDetails){
		
		logger.info("ENTERED FOR LOGIN USER :"+loginDetails.getPassword());
		UserInfo userDetails =userInfoService.login(loginDetails);
		return userDetails;	
	}

}
