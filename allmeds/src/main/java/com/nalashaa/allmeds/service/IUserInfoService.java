package com.nalashaa.allmeds.service;

import com.nalashaa.allmeds.model.Login;
import com.nalashaa.allmeds.model.UserInfo;

/**
 * 
 * @author ashwanikannojia
 *
 *This is interface which is responsible for userInfo services.
 */
public interface IUserInfoService {

	UserInfo register(UserInfo userInfo);

	UserInfo login(Login loginDetails);

}
