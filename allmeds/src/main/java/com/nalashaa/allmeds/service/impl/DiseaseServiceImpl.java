package com.nalashaa.allmeds.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nalashaa.allmeds.model.DiseaseInfo;
import com.nalashaa.allmeds.repository.IDiseaseInfoRepository;
import com.nalashaa.allmeds.service.IDiseaseService;

@Service
public class DiseaseServiceImpl implements IDiseaseService {

	
	@Autowired
	IDiseaseInfoRepository diseaseInfoRepository;
	
	@Override
	public List<DiseaseInfo> getAllDisease() {
		return diseaseInfoRepository.findAll();
	}

}
