/**
 * 
 */
package com.nalashaa.allmeds.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nalashaa.allmeds.model.CompositionDetails;
import com.nalashaa.allmeds.model.DiseaseInfo;
import com.nalashaa.allmeds.model.MapDiseaseMedicine;
import com.nalashaa.allmeds.model.MapMedicineComposition;
import com.nalashaa.allmeds.model.MedicineInfo;
import com.nalashaa.allmeds.model.MedicineInfoResponse;
import com.nalashaa.allmeds.model.PrescriptionDetails;
import com.nalashaa.allmeds.model.UserInfo;
import com.nalashaa.allmeds.repository.ICompositionDetailsRepository;
import com.nalashaa.allmeds.repository.IDiseaseInfoRepository;
import com.nalashaa.allmeds.repository.IMapDiseaseMedicineRepository;
import com.nalashaa.allmeds.repository.IMapMedicineCompositionRepository;
import com.nalashaa.allmeds.repository.IMedicineInfoRepository;
import com.nalashaa.allmeds.repository.IUserInfoRepository;
import com.nalashaa.allmeds.service.IMedicineInfoService;

/**
 * @author ashwanikannojia
 *
 */
@Service
@Transactional
public class MedicineInfoServiceImpl implements IMedicineInfoService {

	@Autowired
	IUserInfoRepository userInfoRepository ;
	
	@Autowired
	IDiseaseInfoRepository diseaseInfoRepository;
	
	@Autowired
	IMapDiseaseMedicineRepository mapDiseaseMedicineRepository; 
	
	@Autowired
	IMedicineInfoRepository medicineInfoRepository;
	
	@Autowired
	IMapMedicineCompositionRepository mapMedicineCompositionRepository; 
	
	@Autowired
	ICompositionDetailsRepository compositionDetailsRepository;
	
	@Override
	public MedicineInfoResponse findPreferMedicine(long userId, String disease) {
		UserInfo user = userInfoRepository.findByUserId(userId);
		DiseaseInfo diseaseInfo = diseaseInfoRepository.findByDiseaseName(disease);
		List<MapDiseaseMedicine> diseaseMedicineList = mapDiseaseMedicineRepository.findByDiseaseId(diseaseInfo.getId());
		List<MedicineInfo> list = new ArrayList<>();
		for (MapDiseaseMedicine mapDiseaseMedicine : diseaseMedicineList){
			list.add(medicineInfoRepository.findByMedicineId(mapDiseaseMedicine.getMedicineId()));
		}
		Map<Long,List<CompositionDetails>> mapOfMedicineIdAndCompositionDetails=new HashMap<>();
		for(MedicineInfo medicineInfo :list) {
			List<MapMedicineComposition> mapMedicineCompositionList= mapMedicineCompositionRepository.findByMedicineId(medicineInfo.getId());
			List<CompositionDetails> listOfCompositionDetails=new ArrayList<>();
				for(MapMedicineComposition mapMedicineComposition : mapMedicineCompositionList) {
					CompositionDetails compositionDetails=compositionDetailsRepository.findByCompositionDetailsId(mapMedicineComposition.getCompositionId());
					listOfCompositionDetails.add(compositionDetails);
				}
				mapOfMedicineIdAndCompositionDetails.put(medicineInfo.getId(), listOfCompositionDetails);
		}
		MedicineInfoResponse medicineInfoResponse=new MedicineInfoResponse();
		medicineInfoResponse.setDisease(disease);
		medicineInfoResponse.setLevel(diseaseInfo.getLevel());
		List<PrescriptionDetails> listOfPrescriptionDetails=new ArrayList<>();
		if(user!=null&&user.getAllergic()!=null) {
			Set<Long> keysSet=mapOfMedicineIdAndCompositionDetails.keySet();
			for(Long medicineId :  keysSet) {
				List<CompositionDetails> listOfIngredients=mapOfMedicineIdAndCompositionDetails.get(medicineId);
				PrescriptionDetails prescriptionDetails=new PrescriptionDetails();
				for(MedicineInfo medicineInfo:list) {
					if(medicineId==medicineInfo.getId()) {
						prescriptionDetails.setMedicineName(medicineInfo.getName());
						break;
					}
				}
				prescriptionDetails.setCompositionList(listOfIngredients);
				if(listOfIngredients.contains(new CompositionDetails(user.getAllergic()))) {
					prescriptionDetails.setRemarks("PRESCRIPTION REQUIRED");
				}
				listOfPrescriptionDetails.add(prescriptionDetails);
			}
		}
		medicineInfoResponse.setListOfPrescriptionDetails(listOfPrescriptionDetails);
		return medicineInfoResponse;
	}
	
	

}
