package com.nalashaa.allmeds.service;

import java.util.List;

import com.nalashaa.allmeds.model.DiseaseInfo;

public interface IDiseaseService {

	List<DiseaseInfo> getAllDisease();

}
