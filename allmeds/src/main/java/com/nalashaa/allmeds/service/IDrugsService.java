/**
 * 
 */
package com.nalashaa.allmeds.service;

import com.nalashaa.allmeds.model.DrugsResponse;

/**
 * @author ashwanikannojia
 *
 */
public interface IDrugsService {

	DrugsResponse isValidDrugs(String drugName, String qrbarcode);

}
