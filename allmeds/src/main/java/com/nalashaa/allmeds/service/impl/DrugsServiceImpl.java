/**
 * 
 */
package com.nalashaa.allmeds.service.impl;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nalashaa.allmeds.model.Drugs;
import com.nalashaa.allmeds.model.DrugsDetails;
import com.nalashaa.allmeds.model.DrugsResponse;
import com.nalashaa.allmeds.repository.IDrugsRepository;
import com.nalashaa.allmeds.service.IDrugsService;

/**
 * @author ashwanikannojia
 *
 */
@Service
public class DrugsServiceImpl implements IDrugsService {

	@Autowired
	IDrugsRepository drugRepository;
	
	@Override
	public DrugsResponse isValidDrugs(String drugName, String qrbarcode) {
		DrugsResponse drugsResponse = null;
		Drugs savedDrugs=drugRepository.findByName(drugName);
		if(null==savedDrugs) {
			return getInvalidDrugs();
		}
		 Set<DrugsDetails> drugsDetailsList=savedDrugs.getDrugsList();
		 Set<DrugsDetails> filtered =null;
		 if(null!=drugsDetailsList) {
			 filtered= drugsDetailsList.stream()
             .filter(dd -> dd.getQrBarCode().equals(qrbarcode))
             .collect(Collectors.toSet());
		 }
		 if(filtered!=null) {
			 return validateExpDate(filtered,drugsResponse);
		 }
		return drugsResponse;
	}

	private DrugsResponse getInvalidDrugs() {
		DrugsResponse drugsResponse=new DrugsResponse();
		drugsResponse.setValid(false);
		return drugsResponse;
		
	}

	private DrugsResponse validateExpDate(Set<DrugsDetails> filtered,DrugsResponse drugsResponse) {
		
			for(DrugsDetails drugDetails :filtered) {
				int comparison=drugDetails.getExpiringDate().compareTo(new Date());
				drugsResponse=new DrugsResponse();
				drugsResponse.setDrugsDetails(drugDetails);
				drugsResponse.setValid(comparison>=0?true:false);
				return drugsResponse;
			}
		return drugsResponse;
	}

}
