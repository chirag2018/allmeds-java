package com.nalashaa.allmeds.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nalashaa.allmeds.model.Login;
import com.nalashaa.allmeds.model.UserInfo;
import com.nalashaa.allmeds.repository.IUserInfoRepository;
import com.nalashaa.allmeds.service.IUserInfoService;

/**
 * @author ashwanikannojia
 *
 */
@Service
@Transactional
public class UserInfoServiceImpl implements IUserInfoService {

	 @Autowired
	 private IUserInfoRepository userInfoRepository;
	 
	 
	/* (non-Javadoc)
	 * @see com.nalashaa.ticketinghelpdesk.controller.IUserInfoService#register(com.nalashaa.ticketinghelpdesk.controller.UserInfo)
	 */
	@Override
	public UserInfo register(UserInfo userInfo) {
		return userInfoRepository.save(userInfo);
	}


	@Override
	public UserInfo login(Login loginDetails) {
		UserInfo user=userInfoRepository.findByUserNameAndPassword(loginDetails.getUserName(), loginDetails.getPassword());
		return user;
	}

}
