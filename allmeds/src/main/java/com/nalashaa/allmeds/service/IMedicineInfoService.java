package com.nalashaa.allmeds.service;
import com.nalashaa.allmeds.model.MedicineInfoResponse;

/**
 * @author ashwanikannojia
 *
 */
public interface IMedicineInfoService {

	MedicineInfoResponse findPreferMedicine(long userId, String disease);

}
