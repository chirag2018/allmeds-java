/**
 * 
 */
package com.nalashaa.allmeds.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author ashwanikannojia
 *
 */
public class PrescriptionDetails implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String medicineName;
	
	private List<CompositionDetails> compositionList;
	
	private String remarks;

	/**
	 * @return the medicineName
	 */
	public String getMedicineName() {
		return medicineName;
	}

	/**
	 * @param medicineName the medicineName to set
	 */
	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}

	/**
	 * @return the compositionList
	 */
	public List<CompositionDetails> getCompositionList() {
		return compositionList;
	}

	/**
	 * @param compositionList the compositionList to set
	 */
	public void setCompositionList(List<CompositionDetails> compositionList) {
		this.compositionList = compositionList;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
