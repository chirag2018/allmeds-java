/**
 * 
 */
package com.nalashaa.allmeds.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ashwanikannojia
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DrugsResponse implements Serializable {

	private static final long serialVersionUID = -6684753200437005125L;

	private DrugsDetails drugsDetails;
	
	private boolean isValid;

	/**
	 * @return the drugsDetails
	 */
	public DrugsDetails getDrugsDetails() {
		return drugsDetails;
	}

	/**
	 * @param drugsDetails the drugsDetails to set
	 */
	public void setDrugsDetails(DrugsDetails drugsDetails) {
		this.drugsDetails = drugsDetails;
	}

	/**
	 * @return the isValid
	 */
	public boolean isValid() {
		return isValid;
	}

	/**
	 * @param isValid the isValid to set
	 */
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	
}
