/**
 * 
 */
package com.nalashaa.allmeds.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ashwanikannojia
 *
 */
@Entity
@Table(name = "MEDICINEINFO")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MedicineInfo implements Serializable{

	private static final long serialVersionUID = -927504085586915367L;

	@Id
    @Column(name = "ID", unique = true, nullable = false, length = 20)
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "NAME", updatable = true, length = 45)
	private String name;
	
	@Column(name = "DESCRIPTION", updatable = true, length = 45)
	private String description;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
