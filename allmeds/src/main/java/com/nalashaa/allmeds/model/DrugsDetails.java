/**
 * 
 */
package com.nalashaa.allmeds.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author ashwanikannojia
 *
 */

@Entity
@Table(name = "DRUGS_DETAILS")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DrugsDetails implements Serializable{
	
	private static final long serialVersionUID = 9121195200047363279L;

	@Id
    @Column(name = "ID", unique = true, nullable = false, length = 20)
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "STRIP_NO", updatable = true, length = 45)
	private long stripNumber;
	
	@Column(name = "QR_BAR_CODE", length = 45)
	private String qrBarCode;
	
	@Column(name = "EXPIRING_DATE")
	private Date expiringDate;
	
	@Column(name = "MANUFACTURING_DATE")
	private Date manufacturingDate;
	
	@ManyToOne
	@JsonBackReference
	private Drugs drug;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the stripNumber
	 */
	public long getStripNumber() {
		return stripNumber;
	}

	/**
	 * @param stripNumber the stripNumber to set
	 */
	public void setStripNumber(long stripNumber) {
		this.stripNumber = stripNumber;
	}

	/**
	 * @return the qrBarCode
	 */
	public String getQrBarCode() {
		return qrBarCode;
	}

	/**
	 * @param qrBarCode the qrBarCode to set
	 */
	public void setQrBarCode(String qrBarCode) {
		this.qrBarCode = qrBarCode;
	}

	/**
	 * @return the expiringDate
	 */
	public Date getExpiringDate() {
		return expiringDate;
	}

	/**
	 * @param expiringDate the expiringDate to set
	 */
	public void setExpiringDate(Date expiringDate) {
		this.expiringDate = expiringDate;
	}

	/**
	 * @return the manufacturingDate
	 */
	public Date getManufacturingDate() {
		return manufacturingDate;
	}

	/**
	 * @param manufacturingDate the manufacturingDate to set
	 */
	public void setManufacturingDate(Date manufacturingDate) {
		this.manufacturingDate = manufacturingDate;
	}

	/**
	 * @return the drug
	 */
	public Drugs getDrug() {
		return drug;
	}

	/**
	 * @param drug the drug to set
	 */
	public void setDrug(Drugs drug) {
		this.drug = drug;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((drug == null) ? 0 : drug.hashCode());
		result = prime * result + ((expiringDate == null) ? 0 : expiringDate.hashCode());
		result = prime * result + ((manufacturingDate == null) ? 0 : manufacturingDate.hashCode());
		result = prime * result + ((qrBarCode == null) ? 0 : qrBarCode.hashCode());
		result = prime * result + (int) (stripNumber ^ (stripNumber >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DrugsDetails other = (DrugsDetails) obj;
		if (drug == null) {
			if (other.drug != null)
				return false;
		} else if (!drug.equals(other.drug))
			return false;
		if (expiringDate == null) {
			if (other.expiringDate != null)
				return false;
		} else if (!expiringDate.equals(other.expiringDate))
			return false;
		if (manufacturingDate == null) {
			if (other.manufacturingDate != null)
				return false;
		} else if (!manufacturingDate.equals(other.manufacturingDate))
			return false;
		if (qrBarCode == null) {
			if (other.qrBarCode != null)
				return false;
		} else if (!qrBarCode.equals(other.qrBarCode))
			return false;
		if (stripNumber != other.stripNumber)
			return false;
		return true;
	}
	
	
	
}
