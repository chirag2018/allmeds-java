package com.nalashaa.allmeds.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "USERINFO")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserInfo implements Serializable {

	private static final long serialVersionUID = 6001704404178947838L;

	@Id
    @Column(name = "ID", unique = true, nullable = false, length = 20)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "USERNAME", updatable = true, length = 45)
	private String userName;
	
	@Column(name = "PASSWORD", updatable = true, length = 45)
	private String password;
	
	@Column(name = "NAME", updatable = true, length = 45)
	private String name;
	
	@Column(name = "DOB", nullable = true)
	private Date dob;
	
	@Column(name = "GENDER", updatable = true, length = 45)
	private String gender;
	
	@Column(name = "ALLERGIC", updatable = true, length = 45)
	private String allergic;
	
	@Column(name = "HEIGHT", updatable = true, length = 45)
	private String height;
	
	@Column(name = "WEIGHT", updatable = true, length = 45)
	private String weight;
	
	@Column(name = "ADDRESS", updatable = true, length = 45)
	private String address;
	
	@Column(name = "CREATEDDATE", nullable = true)
	private Date createdDate;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the allergic
	 */
	public String getAllergic() {
		return allergic;
	}

	/**
	 * @param allergic the allergic to set
	 */
	public void setAllergic(String allergic) {
		this.allergic = allergic;
	}

	/**
	 * @return the height
	 */
	public String getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * @return the weight
	 */
	public String getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(String weight) {
		this.weight = weight;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
