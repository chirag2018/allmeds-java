package com.nalashaa.allmeds.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
@Entity
@Table(name = "DISEASE_INFO")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DiseaseInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
    @Column(name = "ID", unique = true, nullable = false, length = 20)
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "DISEASENAME", unique = true,  nullable = false, updatable = true, length = 45)
	private String diseaseName;
	
	
	@Column(name = "LEVEL")
	private Long level;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the diseaseName
	 */
	public String getDiseaseName() {
		return diseaseName;
	}
	/**
	 * @param diseaseName the diseaseName to set
	 */
	public void setDiseaseName(String diseaseName) {
		this.diseaseName = diseaseName;
	}
	/**
	 * @return the level
	 */
	public Long getLevel() {
		return level;
	}
	/**
	 * @param level the level to set
	 */
	public void setLevel(Long level) {
		this.level = level;
	}
	
}
