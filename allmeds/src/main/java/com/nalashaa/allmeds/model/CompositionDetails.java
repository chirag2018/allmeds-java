package com.nalashaa.allmeds.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "COMPOSITION_DETAILS")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CompositionDetails implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name = "ID", unique = true, nullable = false, length = 20)
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "COMPOSITION_NAME", updatable = true, length = 45)
	private String compositionName;
	
	public CompositionDetails() {
		super();
	}

	public CompositionDetails(String compositionName) {
		super();
		this.compositionName = compositionName;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the compositionName
	 */
	public String getCompositionName() {
		return compositionName;
	}

	/**
	 * @param compositionName the compositionName to set
	 */
	public void setCompositionName(String compositionName) {
		this.compositionName = compositionName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((compositionName == null) ? 0 : compositionName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompositionDetails other = (CompositionDetails) obj;
		if (compositionName == null) {
			if (other.compositionName != null)
				return false;
		} else if (!compositionName.equals(other.compositionName))
			return false;
		return true;
	}

}
