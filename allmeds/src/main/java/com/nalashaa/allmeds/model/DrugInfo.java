/**
 * 
 */
package com.nalashaa.allmeds.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author ashwanikannojia
 *
 */
public class DrugInfo implements Serializable {
	
	private String drugName;
	
	private List<String> ingredients;

	/**
	 * @return the drugName
	 */
	public String getDrugName() {
		return drugName;
	}

	/**
	 * @param drugName the drugName to set
	 */
	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	/**
	 * @return the ingredients
	 */
	public List<String> getIngredients() {
		return ingredients;
	}

	/**
	 * @param ingredients the ingredients to set
	 */
	public void setIngredients(List<String> ingredients) {
		this.ingredients = ingredients;
	}
	
}
