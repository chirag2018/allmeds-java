package com.nalashaa.allmeds.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "MAP_DISEASE_MEDICINE")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MapDiseaseMedicine implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @Column(name = "ID", unique = true, nullable = false, length = 20)
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "MEDICINE_ID", unique = true,  nullable = false, updatable = true, length = 45)
	private Long medicineId;
	
	@Column(name = "DISEASE_ID", unique = true,  nullable = false, updatable = true, length = 45)
	private Long diseaseId;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the medicineId
	 */
	public Long getMedicineId() {
		return medicineId;
	}
	/**
	 * @param medicineId the medicineId to set
	 */
	public void setMedicineId(Long medicineId) {
		this.medicineId = medicineId;
	}
	/**
	 * @return the diseaseId
	 */
	public Long getDiseaseId() {
		return diseaseId;
	}
	/**
	 * @param diseaseId the diseaseId to set
	 */
	public void setDiseaseId(Long diseaseId) {
		this.diseaseId = diseaseId;
	}
	

}
