package com.nalashaa.allmeds.model;

import java.io.Serializable;
import java.util.List;

public class MedicineInfoResponse implements Serializable{

	private static final long serialVersionUID = 238626336693345467L;

	private String disease;
	
	private List<PrescriptionDetails> listOfPrescriptionDetails;
	
	private String DosAndDonts;
	
	private long level;

	/**
	 * @return the disease
	 */
	public String getDisease() {
		return disease;
	}

	/**
	 * @param disease the disease to set
	 */
	public void setDisease(String disease) {
		this.disease = disease;
	}

	/**
	 * @return the listOfPrescriptionDetails
	 */
	public List<PrescriptionDetails> getListOfPrescriptionDetails() {
		return listOfPrescriptionDetails;
	}

	/**
	 * @param listOfPrescriptionDetails the listOfPrescriptionDetails to set
	 */
	public void setListOfPrescriptionDetails(List<PrescriptionDetails> listOfPrescriptionDetails) {
		this.listOfPrescriptionDetails = listOfPrescriptionDetails;
	}

	/**
	 * @return the dosAndDonts
	 */
	public String getDosAndDonts() {
		return DosAndDonts;
	}

	/**
	 * @param dosAndDonts the dosAndDonts to set
	 */
	public void setDosAndDonts(String dosAndDonts) {
		DosAndDonts = dosAndDonts;
	}

	/**
	 * @return the level
	 */
	public long getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(long level) {
		this.level = level;
	}
	
}
