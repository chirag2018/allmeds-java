/**
 * 
 */
package com.nalashaa.allmeds.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author ashwanikannojia
 *
 */

@Entity(name = "ForeignKeyAssoEntity")
@Table(name = "DRUGS")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Drugs implements Serializable {

	private static final long serialVersionUID = 4725743237101287929L;

	@Id
    @Column(name = "ID", unique = true, nullable = false, length = 20)
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "NAME", unique = true,  nullable = false, updatable = true, length = 45)
	private String name;
	
	@Column(name = "DESCRIPTION", updatable = true, length = 45)
	private String description;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="DRUG_ID")
	@JsonManagedReference
	private Set<DrugsDetails> drugsList = new HashSet<>();

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the drugsList
	 */
	public Set<DrugsDetails> getDrugsList() {
		return drugsList;
	}

	/**
	 * @param drugsList the drugsList to set
	 */
	public void setDrugsList(Set<DrugsDetails> drugsList) {
		this.drugsList = drugsList;
	}
	
}
